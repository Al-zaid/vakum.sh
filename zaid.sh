#!/bin/bash
#version 1.0
# Variables
b='\033[1m'
u='\033[4m'
bl='\E[30m'
r='\E[31m'
g='\E[32m'
bu='\E[34m'
m='\E[35m'
c='\E[36m'
w='\E[37m'
endc='\E[0m'
enda='\033[0m'
blue='\e[1;34m'
cyan='\e[1;36m'
red='\e[1;31m'

figlet Al-zaid | lolcat
echo  "_____________________________________________________________" | lolcat
echo  "Tools    : Al-zaid $white " |lolcat
echo  "TEAM  : ICC $white   " |lolcat
echo  "Status  : Vakum $white   " |lolcat
echo  "Contact  : adibondrat01@gmail.com $white " |lolcat
echo  "_____________________________________________________________" | lolcat

###################################################
# CTRL + C
###################################################
trap ctrl_c INT
ctrl_c() {
clear
echo -e $red"[#]> (Ctrl + C ) Detected, Trying To Exit ... "
echo -e $cyan"[#]> Thanks"
sleep 1
echo ""
echo -e $white"[#]> see you gaes :)..."
sleep 1
exit
}

lagi=1
while [ $lagi -lt 6 ];
do
echo ""
echo   "1.  Nmap";
echo  "============================" | lolcat
echo   "2.  Admin-finder";
echo  "============================" | lolcat
echo   "3. Tebas Index Live Target";
echo  "============================" | lolcat
echo   "4   Lazymux";
echo  "============================" | lolcat
echo   "5.  Tools-X";
echo  "============================" | lolcat
echo   "6.  Wifi Hacker";
echo  "============================" | lolcat
echo   "7.  Thorshammer";
echo  "============================" | lolcat
echo   "8.  IPGeoLocation";
echo  "============================" | lolcat
echo   "9.  Spam Mirror Defacer.id";
echo  "============================" | lolcat
echo   "10. OSIF";
echo  "============================"
echo   "11. LazySqlMap";
echo  "============================" | lolcat
echo   "12.  Exit";
echo ""
echo "Silahkan Pilih Nomor Guys" |lolcat
read -p "Nomor Berapa Kymac:" pil;

# Nmap

case $pil in
1) pkg install nmap
echo -e  "${y} {1} Masukkan Web:"
read web
nmap $web
echo

;;

# admin-finder

2) git clone  https://github.com/the-c0d3r/admin-finder.git
echo -e "${y} cara menggunakan admin finder" | lolcat
echo -e "${y} cd admin-finder" | lolcat
echo -e "${y} python admin-finder.py" | lolcat
cd /data/data/com.termux/files/home/admin-finder/
python2 /data/data/com.termux/files/home/admin-finder/admin-finder

;;

#Tebas Index Live Target

3) git clone https://github.com/21D4N404/Defacer
echo -e "${y} Installer Defacer..." | lolcat
echo -e "${y} cd Defacer" | lolcat
echo -e "${y} sh ICA.sh" | lolcat
cd /data/data/com.termux/files/home/Defacer/
sh /data/data/com.termux/files/home/ICA/ ICA.sh

;;

#Lazymux

4) git clone https://github.com/Gameye98/Lazymux
echo -e "${y} Installer Lazymux..." | lolcat
echo -e "${y} cd Lazymux" | lolcat
echo -e "${y} python lazymux.py" | lolcat
cd /data/data/com.termux/files/home/Lazymux/
python2 /data/data/com.termux/files/home/Lazymux/ lazymux.py

;;

#Tools-X

5) git clone https://github.com/Rajkumrdusad/Tool-X
echo -e "${y} Installer Tool-X..." | lolcat
echo -e "${y} cd Tool-X" | lolcat
echo -e "${y} sh install.aex" | lolcat
cd /data/data/com.termux/files/home/Tool-X
bash /data/data/com.termux/files/home/Tool-X/sh install.aex

;;

#Wifi-Hacker

6)git clone https://github.com/esc0rtd3w/wifi-hacker
echo -e "${y} Installer Wifi-Hacker..." | lolcat
echo -e "${y} cd wifi-hacker" | lolcat
echo -e "${y} pip2 install -r requirements.txt" | lolcat
echo -e "${y} ./wifi-hacker" | lolcat
echo -e "${y} Ketik Y" | lolcat
echo -e "${y} Hidupkan Wifi Kalian" | lolcat
echo -e "${y} Trus Pilih Nomor 2" | lolcat
cd /data/data/com.termux/files/home/Wifi-Hacker/
bash /data/data/com.termux/files/home/Wifi-Hacker/ wifi-hacker.sh

;;

#Torshammer

7)git clone https://github.com/dotfighter/torshammer.git 
echo -e "${y} Installer Thorshammer..." | lolcat
echo -e "${y} cd thorshammer" | lolcat
echo -e "${y} python2 torshammer.py -t (web yang mau kalian attack) tanpa tanda kurung" | lolcat
cd /data/data/com.termux/files/home/Thorshammer/
bash /data/data/com.termux/files/home/Thorshammer/ thorshammer.py

;;

#IPGeoLocation

8)git clone https://github.com/maldevel/IPGeoLocation
echo -e "${y} Installer IPGeoLocation..." | lolcat
echo -e "${y} cd IPGeoLocation" | lolcat
echo -e "${y} python ipgeolocation.py -m " | lolcat
echo -e "${y} python ipgeolocation.py -h" | lolcat
echo -e "s{y} python ipgeolocation.py -t target ip kalian yg mau dilacak" | lolcat
cd /data/data/com.termux/files/home/IPGeoLocation/
bash /data/data/com.termux/files/home/IPGeoLocation/ ipgeolocation.py	

;;	

#Spam Mirror Defacer.id

9)git clone https://github.com/soracyberteam/the-fake/
echo -e "${y} Installer the-fake..." | lolcat
echo -e "${y} cd the-fake" | lolcat
echo -e "${y} sh fake.sh" | lolcat
cd /data/data/com.termux/files/home/the-fake/
sh /data/data/com.termux/files/home/fake/ fake.sh

;;

#OSIF

10)git clone https://github.com/ciku370/OSIF
echo -e "${y} Installer OSIF..." | lolcat
echo -e "${y} cd OSIF" | lolcat
echo -e "${y} python2 osif.py" | lolcat
echo -e "${y} get_token" | lolcat
echo -e "${y} dump_mail (untuk lihat email)" | lolcat
echo -e "${y} dump_phone (untuk melihat nomor telepon)" | lolcat
cd /data/data/com.termux/files/home/OSIF/
bash /data/data/com.termux/files/home/OSIF/ osif.py

;;

#LazySqlMap

11)git clone https://github.com/verluchie/termux-lazysqlmap.git
echo -e "${y} Installer termux-lazysqlmap..." | lolcat
echo -e "${y} cd termux-lazysqlmap" | lolcat
echo -e "${y} chmod 777 install.sh" | lolcat
echo -e "${y} ./install.sh" | lolcat
cd /data/data/com.termux/files/home/termux-lazysqlmap/
.//data/data/com.termux/files/home/install/install.sh

;;

12) echo "created by : Al-zaid" | lolcat
echo "Btw gw gans ea Tq" | lolcat
echo "Thanks for TEAM ICC" | lolcat
echo "di Install Tadi Ketik cd $HOME" | lolcat
exit

;;

*) echo "sorry, pilihan yang anda cari tidak ada di karenakan anda jomblo"
esac
done
done